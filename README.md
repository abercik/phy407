# README #

# Here I include all the problem sets from my computational physics course, PHY407 at the University of Toronto, 2018 Fall semester.
# This python based course explores computational techniques with direct applications to physics. Problem sets were done with my partner, Jaewon Yun.

# https://www.physics.utoronto.ca/students/undergraduate-courses/current/phy407h1
