# SolveLinear.py
# Python module for PHY407
# Paul Kushner, 2015-09-26
# Modifications by Nicolas Grisouard, 2018-09-26
# This module contains useful routines for solving linear systems of equations.
# Based on gausselim.py from Newman
# The following will be useful for partial pivoting
from numpy import empty, copy

def GaussElim(A_in, v_in, pivot=False):
    """Implement Gaussian Elimination. This should be non-destructive for input
    arrays, so we will copy A and v to
    temporary variables
    IN:
    A_in, the matrix to pivot and triangularize
    v_in, the RHS vector
    pivot: boolean flag, set True to do partial pivoting, default = False
    OUT:
    x, the vector solution of A_in x = v_in """
    # copy A and v to temporary variables using copy command
    A = copy(A_in)
    v = copy(v_in)
    N = len(v)

    for m in range(N):
        # Divide by the diagonal element
        if pivot == True: PartialPivot(A,v,m)
        div = A[m, m]
        A[m, :] /= div
        v[m] /= div

        # Now subtract from the lower rows
        for i in range(m+1, N):
            mult = A[i, m]
            A[i, :] -= mult*A[m, :]
            v[i] -= mult*v[m]

    # Backsubstitution
    # create an array of the same type as the input array
    x = empty(N, dtype=v.dtype)
    for m in range(N-1, -1, -1):
        x[m] = v[m]
        for i in range(m+1, N):
            x[m] -= A[m, i]*x[i]
    return x


def PartialPivot(A, v, i):
    """ Perform Partial Pivoting of a Matrix 
    INPUT: Matrix, vector for which to solve, index of current row
    OUTPUT: nothing (simply modifies input Matrix)
    i = index of row on which to do the partial pivoting
    """
    N = len(v) # Save dimension of vector
    k = i + 1 # Set counter to check all rows starting at index below ith row
    
    # We now compare the mth element of the mth row with all other mth elements
    # in the rows below. End when we reach the end (index + 1 = dim(v))
    while k < N:
        if (abs(A[i,i]) < abs(A[k,i])):
        # if the lower row has a larger mth element value, switch the two rows
            A[i,:], A[k,:] = copy(A[k,:]), copy(A[i,:]) # switch matrix
            v[i], v[k] = copy(v[k]), copy(v[i]) # switch vector
        k += 1 # increment counter

